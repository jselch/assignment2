/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;


import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;


public class MasterProblem {
    
    private final IloCplex model;
    private final IloIntVar u[][];
    private final IloNumVar c[][];
    private final IloNumVar phi;
    private final UnitCommitmentProblem ucp;
    
//We create the Masterproblem by fixing u_gh and c_gh. If we fix the mentioned variables the 
//problem becomes a LP problem instead of an MIP problem, which is easier to solve     
    public MasterProblem(UnitCommitmentProblem ucp)throws IloException{
        
        
        this.ucp = ucp; 
        
// We then create an IloCplex object that contains the model
        this.model = new IloCplex();
        
//We import the decisionsvariables from the UnitCommitmentModel
        // The u_gt variables
        u = new IloIntVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                u[g-1][h-1] = model.boolVar("u_"+g+"_"+h);
            }
        }
        
        // The c_gt variables
        c = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                c[g-1][h-1] = model.numVar(0,Double.POSITIVE_INFINITY,"c_"+g+"_"+h);
            }
        }
        
        /* We then create phi which will go into the objective 
        * function of the Master Problem
        */
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");  
        
        
        /* 
        * After this we create the objective function of the Master Problem containing u and c.
        * At first we create it as an empty linear numerical expression 
        */
        IloLinearNumExpr obj = model.linearNumExpr();
     
    // We start adding terms to the objective function
    
    //We add the first double sum
    for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours() ; h++){
                obj.addTerm(1, c[g-1][h-1]);
                obj.addTerm(ucp.getCommitmentCost(g), u[g-1][h-1]);
            }
    }
    
    // Phi is added to the objective function
    obj.addTerm(1, phi);
        
    //We use CPLEX to minimize the objective function
    model.addMinimize(obj);
    
    
    /*************** CONSTRAINTS IN THE MASTER PROBLEM ***************/
 
    
    /* In this next section we create the constraints to the Master Problem.
    * We add the constraints by creating linear equations with IloLinearNumExpr for each constraint.
    * The MP will include constraints 1.b, 1.c and 1.d.
    */
    
    // Constraint 1b taken from UnitCommitmentModel
    for(int h = 1; h <= ucp.getnHours(); h++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                lhs.addTerm(1, c[g-1][h-1]);
                // We bring the right-hand-side to the lhs, changing sign
                lhs.addTerm(-ucp.getStartUpCost(g), u[g-1][h-1]);
                if(h > 1){
                    lhs.addTerm(+ucp.getStartUpCost(g), u[g-1][h-2]); // Note that in order to get u_g,t-1 we need to access u[g-1][t-2] (notice the -2)                
                }
                // Finally we add the constraint
                model.addGe(lhs, 0);
            }
        }
    
    //Constraint 1c taken from UnitCommitmentModel
    for(int h = 1; h <= ucp.getnHours(); h++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over h'
                for(int hh = h; hh <= ucp.getMinOnTimeAtH(g, h); hh++){
                    lhs.addTerm(1, u[g-1][hh-1]);
                    lhs.addTerm(-1, u[g-1][h-1]);
                    if(h > 1){
                        lhs.addTerm(1, u[g-1][h-2]);
                    }
                }
                model.addGe(lhs,0);
            }
        }
    
    // Constraint 1d taken from UnitCommitmentModel
        for(int h = 1; h <= ucp.getnHours(); h++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs = model.linearNumExpr();
                // We loop over h'
                // We add one every time we loop. We save this sum in a constant
                int constant = 0;
                for(int hh = h; hh <= ucp.getMinOffTimeAtH(g, h); hh++){
                    constant++;
                    lhs.addTerm(-1, u[g-1][hh-1]);
                    lhs.addTerm(1, u[g-1][h-1]);
                    if(h > 1){
                        lhs.addTerm(-1, u[g-1][h-2]);
                    }
                }
                model.addGe(lhs,constant);
            }
        }
    
    }
       // We use CPLEX to solve the Master Problem.
        public void solve() throws IloException{    


      /* We follow the code from https://bitbucket.org/teachingcode/
       * integerbendersdecomposition/src/master/benders/MasterProblem.java to create
       * the method for Benders Decomposition in Java
       */     
        model.use(new OurCallback());
        
        model.solve();
    }
    
    private class OurCallback extends IloCplex.LazyConstraintCallback{
 
        public OurCallback() {
        }
 
        @Override
        protected void main() throws IloException {
            
            double[][] U = getU();
            double Phi = getPhi();
            
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(ucp, U);
            fsp.solve();
            double fspObjective = fsp.getObjective();

            System.out.println("FSP "+fspObjective);
            if(fspObjective >= 0+1e-9){
                System.out.println("Generating feasibility cut");
                double constant = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(u);           
                IloRange cut = model.le(linearTerm, -constant);
                add(cut);
            }else{
                OptimalitySubProblem osp = new OptimalitySubProblem(ucp,U);
                osp.solve();
                double ospObjective = osp.getObjective();
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    System.out.println("The current node is optimal");
                }else{
                    System.out.println("Generating optimality cut");
                    double cutConstant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(u);
                    cutTerm.addTerm(-1, phi);
                    IloRange cut = model.le(cutTerm, -cutConstant);
                    add(cut);
                }
            }
        }
        
        public double[][] getU() throws IloException{
            double U[][] = new double[ucp.getnGenerators()][ucp.getnHours()];
            for(int g = 1;g <= ucp.getnGenerators(); g++){
                for(int h = 1; h <= ucp.getnHours(); h++){ 
                    U[g-1][h-1] = getValue(u[g-1][h-1]);
                }
            }
            return U;
        }

        public double getPhi() throws IloException{
           return getValue(phi);
        }
    }
    
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    public void printSolution() throws IloException{
        for(int g = 1;g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){ 
                System.out.println( "Generator "+ucp.getGeneratorName(g)+ " in hour " +h+ " = "+model.getValue(u[g-1][h-1]));
            }
        }   
    }
    
    public void end(){
        model.end();
    }
    
}