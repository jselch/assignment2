/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;


import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;

public class FeasibilitySubProblem {
    private final IloCplex model; 
    private final IloNumVar p[][];
    private final IloNumVar l[];
    private final IloNumVar w1[];
    private final IloNumVar w2[][];
    private final IloNumVar w3[][];
    private final IloNumVar w4[][];
    private final IloNumVar w5[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange PowerBalance[];
    private final IloRange MinCapacity[][];
    private final IloRange MaxCapacity[][];
    private final IloRange RampUp[][];
    private final IloRange RampDown[][];
    
   /*
     * We now create the model for the feasibility subproblem.  
     *
     * @throws IloException 
     */
    
    public FeasibilitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
    // We create an IloCplex object, which is the object that will contain 
    // the the model.
        this.model = new IloCplex();
        
    
    /*
    *In this feasibility subproblem p and l will be the decision variables. 
    * Both of these are non-negative continous variables and are therefore 
    * defined as IloNumVar from 0 to positive infinity.
    */
    
        l = new IloNumVar[ucp.getnHours()];
        for(int h = 1; h <= ucp.getnHours(); h++){
            l[h-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l_"+h);
        }
        
     
        p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                p[g-1][h-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p_"+g+"_"+h);
            }
        }
    
    /*
    * Then we create the slack variables in the Feasibility Subproblem. They 
    * are also continouos variables defined from 0 to +infinity, so they are 
    * created similarly to the decision variables. 
    */
    
    /*
    * The next step is to create the slack-variables. These will be created
    * in the same way as l or p since the slacks are also non-negative continous variables.
    */
       w1 = new IloNumVar[ucp.getnHours()];
       for(int h = 1; h <= ucp.getnHours(); h++){
            w1[h-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w1_"+h);
       }
    
        w2 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                w2[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w2_"+h);
            }
        }
        
        w3 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                w3[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w3_"+h);
            }
        }
        
        w4 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                w4[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w4_"+h);
            }
        }
        
        w5 = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){
                w5[g-1][h-1] = model.numVar(0, Double.POSITIVE_INFINITY,"w5_"+h);
            }
        }

    /*************** OBJECTIVE FUNCTION ***************/
    
    // The objective function for the feasibility subproblem is created.
    // We create an empty linear equation that we can add the terms to.
    
    IloLinearNumExpr obj = model.linearNumExpr();
    
    /* We then add the terms to the objetive function. The objective function 
    * will consist of the sum of all the slack variables. One slack variable will only
    * depend on h, whilst the remaing four will depend on both h and g
    */
    
        for(int h = 1; h <= ucp.getnHours(); h++){
                obj.addTerm(1, w1[h-1]);
        }
        for(int g = 1; g <= ucp.getnGenerators() ; g++){
            for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(1, w2[g-1][h-1]);
                obj.addTerm(1, w3[g-1][h-1]);
                obj.addTerm(1, w4[g-1][h-1]);
                obj.addTerm(1, w5[g-1][h-1]);
            }
        }
    
    //We tell CPLEX to minimize the objective function
    model.addMinimize(obj);
    
    
    /****************** CONSTRAINTS ******************/
    
    /*
    * Now we create the constraints in the feasibility subproblem. 
    * The constraints needed in the feasibility subproblem are the 
    * constraints not included in the Master Problem.
    * The consraints have been taken from UnitCommitmentModel
    * and the slack variables have been added.
    
    /* Constraint 1e
    * Usually in an equation with an equality we would have both a positive
    * and a negative slack variable. But since l_h is a positive variable, 
    * we only need to add the negative slack variable in this constraint. 
    */
    this.PowerBalance = new IloRange[ucp.getnHours()]; 
    for(int h = 1; h <= ucp.getnHours(); h++){
            IloLinearNumExpr lhs1e = model.linearNumExpr();
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                lhs1e.addTerm(1, p[g-1][h-1]);
            }
            lhs1e.addTerm(1, l[h-1]);
            lhs1e.addTerm(-1, w1[h-1]);
      PowerBalance[h-1] = model.addEq(lhs1e, ucp.getDemand(h));
    }

    /*Constraint 1f
    * In this constraint we have a greater than or equal to inequality
    * which is why we here add a positive slack variable.
    */
    this.MinCapacity = new IloRange[ucp.getnHours()][ucp.getnGenerators()];
    for(int h = 1; h <= ucp.getnHours(); h++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs1f = model.linearNumExpr();
                lhs1f.addTerm(1,p[g-1][h-1]);
                lhs1f.addTerm(1, w2[g-1][h-1]);
           // Notice that we now use U(g,t) and not u(g,t). U(g,t) is the 
           // solution at the current node in the branch and bound. 
           // In the following constraints we also use U(g,t). 
        MinCapacity[g-1][h-1] = model.addGe(lhs1f,ucp.getMinProduction(g)*U[g-1][h-1]);
        }
    } 


    /*Constraint 1g
    * In this constraint we have a lesser than or equal to inequality
    * which is why we here add a negative slack variable.
    */
    this.MaxCapacity = new IloRange[ucp.getnHours()][ucp.getnGenerators()];
    for(int h = 1; h <= ucp.getnHours(); h++){
            for(int g = 1; g <= ucp.getnGenerators(); g++){
                IloLinearNumExpr lhs1g = model.linearNumExpr();
                lhs1g.addTerm(1,p[g-1][h-1]);
                lhs1g.addTerm(-1,w3[g-1][h-1]);
        MaxCapacity[g-1][h-1] = model.addLe(lhs1g,ucp.getMaxProduction(g)*U[g-1][h-1]);
        }
    } 

    /* Constraint 1h
    * In this constraint we have a lesser than or equal to inequality
    * which is why we here add a negative slack variable.
    */
    this.RampUp = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
    for(int g = 1; g <= ucp.getnGenerators(); g++){
        for(int h = 1; h <= ucp.getnHours(); h++){    
           IloLinearNumExpr lhs1h = model.linearNumExpr();  
           lhs1h.addTerm(1, p[g-1][h-1]);
           if(h > 1){
           lhs1h.addTerm(-1, p[g-1][h-2]);
           }
           lhs1h.addTerm(-1, w4[g-1][h-1]);
        RampUp[g-1][h-1] = model.addLe(lhs1h, ucp.getRampUp(g));
        }
    }    
        
    /* Constraint 1i
    * In this constraint we have a lesser than or equal to inequality
    * which is why we here add a negative slack variable.
    */
    this.RampDown = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
    for(int g = 1;g <= ucp.getnGenerators(); g++){
        for(int h = 1; h <= ucp.getnHours(); h++){    
           IloLinearNumExpr lhs1i = model.linearNumExpr();  
           lhs1i.addTerm(-1, p[g-1][h-1]);
           if(h > 1){
           lhs1i.addTerm(1, p[g-1][h-2]);
           }
           lhs1i.addTerm(-1, w5[g-1][h-1]);
        RampDown[g-1][h-1] = model.addLe(lhs1i, ucp.getRampDown(g));
        } 
    }  
    }

    /*
     * We then tell CPLEX to solve the feasibility subproblem. 
     */
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }

    /*
     * We create a method that returns the objective value of the feasibility 
     * subproblem. 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }

    /*
    * In case the solution from the current node in the branch and bound
    * is not feasible we want to create a feasibility cut.
    * To add this we are going to need the dual variables from all the constraints.
    * We create both a constant and a linear part of the cut. ´
    *
    * The first part of the feasibility cut is constant (does not depend on u). 
    */
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int h = 1; h <= ucp.getnHours() ; h++){
            constant = constant + model.getDual(PowerBalance[h-1]) * ucp.getDemand(h);
        for(int g = 1; g <= ucp.getnGenerators() ; g++){
            constant = constant + model.getDual(RampUp[g-1][h-1])*ucp.getRampUp(g) +
                    model.getDual(RampDown[g-1][h-1])* ucp.getRampDown(g);
        }
    } 
        return constant;
    }
    /*
    * The second part of the feasibility cut is linear in u. It is created as 
    * an empty linear expression that we add terms to. 
    */
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        for(int g = 1;g <= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours(); h++){ 
                cutTerm.addTerm(model.getDual(MinCapacity[g-1][h-1])* ucp.getMinProduction(g), u[g-1][h-1]);
                cutTerm.addTerm(model.getDual(MaxCapacity[g-1][h-1])* ucp.getMaxProduction(g), u[g-1][h-1]);
            }
        }
        return cutTerm;
    }
    
    // We release the resources the IloCplex object uses.
    public void end(){
        model.end();
    }
    
}