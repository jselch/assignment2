package assignment;

import assignment.UnitCommitmentProblem;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;


 /* The optimality subproblem consist of the part of the original problem 
  * that is not included in the Master Problem. 
  */
public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar l[];
    private final IloNumVar p[][];
    private final UnitCommitmentProblem ucp;
    private final IloRange PowerBalance[];
    private final IloRange MinCapacity[][];
    private final IloRange MaxCapacity[][];
    private final IloRange RampUp[][];
    private final IloRange RampDown[][];
    
    
    //We start by creating the model for the optimality subproblem
    public OptimalitySubProblem(UnitCommitmentProblem ucp, double U[][]) throws IloException {
        this.ucp = ucp;
        
                
        this.model = new IloCplex();

    //We create the l_h variables
    l = new IloNumVar[ucp.getnHours()];
    for(int h = 1; h <= ucp.getnHours(); h++){
        l[h-1] = model.numVar(0,Double.POSITIVE_INFINITY,"l_"+h);
    }
          
    //We create the p_ht variables
    p = new IloNumVar[ucp.getnGenerators()][ucp.getnHours()];
    for(int g = 1; g <= ucp.getnGenerators(); g++){
        for(int h = 1; h <= ucp.getnHours(); h++){
            p[g-1][h-1] = model.numVar(0,Double.POSITIVE_INFINITY,"p_"+g+"_"+h);
        }
    }
           
        IloLinearNumExpr obj = model.linearNumExpr();

    for(int h = 1; h<= ucp.getnHours(); h++){
                obj.addTerm(ucp.getSheddingCost(h-1), l[h-1]);
            }
    
    for(int g = 1; g<= ucp.getnGenerators(); g++){
            for(int h = 1; h <= ucp.getnHours() ; h++){
                obj.addTerm(ucp.getProductionCost(g-1), p[g-1][h-1]);
            }
    }

    model.addMinimize(obj);
    

    /****************** CONSTRAINTS ******************/
    //We import the constraints from UnitCommitmentModel with a few modifications
       
    // Constraint 1e
    this.PowerBalance = new IloRange[ucp.getnHours()]; 
    for(int h = 1; h <= ucp.getnHours(); h++){
        IloLinearNumExpr lhs = model.linearNumExpr();
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            lhs.addTerm(1, p[g-1][h-1]);
        }
        lhs.addTerm(1, l[h-1]);
    PowerBalance[h-1] = model.addEq(lhs, ucp.getDemand(h));
    }
    
    // Constraint 1f
    this.MinCapacity = new IloRange[ucp.getnGenerators()][ucp.getnHours()];    
    for(int h = 1; h <= ucp.getnHours(); h++){
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            lhs.addTerm(1,p[g-1][h-1]);
    MinCapacity[g-1][h-1] = model.addGe(lhs,ucp.getMinProduction(g)*U[g-1][h-1]);
        }
    }

    // Constraint 1g
    this.MaxCapacity = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
    for(int h = 1; h <= ucp.getnHours(); h++){
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            lhs.addTerm(1,p[g-1][h-1]);
    MaxCapacity[g-1][h-1] = model.addLe(lhs, ucp.getMaxProduction(g)*U[g-1][h-1]);
        }
    }
    
    // Constraint 1h
    this.RampUp = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
    for(int h = 1; h <= ucp.getnHours(); h++){
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            lhs.addTerm(1,p[g-1][h-1]);
            if(h > 1){
                lhs.addTerm(-1,p[g-1][h-2]);
            }
    RampUp[g-1][h-1] = model.addLe(lhs,ucp.getRampUp(g));
        }
    }

    // Constraint 1i
    this.RampDown = new IloRange[ucp.getnGenerators()][ucp.getnHours()];
    for(int h = 1; h <= ucp.getnHours(); h++){
        for(int g = 1; g <= ucp.getnGenerators(); g++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            if(h > 1){
                lhs.addTerm(1,p[g-1][h-2]);
            }
            lhs.addTerm(-1,p[g-1][h-1]);
    RampDown[g-1][h-1] = model.addLe(lhs,ucp.getRampDown(g));
        }
    }   
}
    //We use CPLEX to solve the optimality subproblem
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }

    //Returns the objective value of the optimality subproblem
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    //In the case that the solution is not optimal we create an optimality cut.
    
    //This part returns the constant part of the optimality cut.
    public double getCutConstant() throws IloException{
    double constant = 0;
        for(int h = 1; h <= ucp.getnHours() ; h++){
        constant = constant + model.getDual(PowerBalance[h-1]) * ucp.getDemand(h);
            for(int g = 1; g <= ucp.getnGenerators() ; g++){
        constant = constant + model.getDual(RampUp[g-1][h-1])*ucp.getRampUp(g) +
                model.getDual(RampDown[g-1][h-1])* ucp.getRampDown(g);
        }
    } 
        return constant;
    }
   
    //This part returns the linear part of the optimality cut
    public IloLinearNumExpr getCutLinearTerm(IloIntVar u[][]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        
    for(int g = 1;g <= ucp.getnGenerators(); g++){
        for(int h = 1; h <= ucp.getnHours(); h++){ 
            cutTerm.addTerm(model.getDual(MinCapacity[g-1][h-1])* ucp.getMinProduction(g), u[g-1][h-1]);
            cutTerm.addTerm(model.getDual(MaxCapacity[g-1][h-1])* ucp.getMaxProduction(g), u[g-1][h-1]);
            }
        }
        return cutTerm;
    }
    
    public void end(){
        model.end();
    }
}