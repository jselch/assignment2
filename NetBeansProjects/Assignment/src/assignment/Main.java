/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment;

import ilog.concert.IloException;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.Scanner;




public class Main {
    public static void main(String[] args) throws FileNotFoundException, IloException{
   
        // 1. READS THE GENERATORS DATA
        // The generators data are stored in the generators.txt file
        // Note that the generators.txt file must be in project's specific NetBeans 
        // directory. If the generators.txt file is elsewhere you will need to provide
        // an absolute path such as "C://Users/Documents/MyFolder/generators.txt"
        File generatorsFile = new File("generators.txt");
        Scanner scanner = new Scanner(generatorsFile).useLocale(Locale.US);
        
        // We skip the first two lines of the file since they are a header
        // That is, we move the cursor past the first two lines (at the beginning of the third)
        scanner.nextLine();
        scanner.nextLine();
        
        // We create the arrays for storing the data we read.
        // These arrays will then be used to create an instance of 
        // the UnitCommitmentProblem class.
        
        // We have 31 generators in the file
        int nGenerators = 31;
        String[] names = new String[nGenerators];
        double[] minProduction = new double[nGenerators];
        double[] maxProduction = new double[nGenerators];
        double[] startUpCosts = new double[nGenerators];
        double[] commitmentCosts = new double[nGenerators];
        double[] rampUp = new double[nGenerators];
        double[] rampDown = new double[nGenerators];
        int[] minUpTime = new int[nGenerators];
        int[] minDownTime = new int[nGenerators];
        double[] productionCost = new double[nGenerators];
        
        for(int i = 1; i <= nGenerators; i++){
            names[i-1] = scanner.next();
            minProduction[i-1] = scanner.nextDouble();
            maxProduction[i-1] = scanner.nextDouble();
            startUpCosts[i-1] = scanner.nextDouble();
            commitmentCosts[i-1] = scanner.nextDouble();
            rampUp[i-1] = scanner.nextDouble();
            // RampDown is equal to RampUp
            rampDown[i-1] = rampUp[i-1];
            minUpTime[i-1] = scanner.nextInt();
            minDownTime[i-1] = scanner.nextInt();
            productionCost[i-1] = scanner.nextDouble();
        }
        
        
        // 2. READS THE LOADS DATA
        // We have 24 hours

        File loadFile = new File("loads.txt");
        Scanner d = new Scanner(loadFile);
        
        // We skip the first line of the file since they are a header
        // That is, we move the cursor past the first line (at the beginning of the third)
        d.nextLine();
        
        int nHours = 24;
        double[] demand = new double[nHours];
        for (int h = 1; h <= nHours; h++){
            demand[h-1] = d.nextDouble();
    }
       
        // We calculate min up time and down time for each t
        int[][] minUpTimeAtH = new int[nGenerators][nHours];
        int[][] minDownTimeAtH = new int[nGenerators][nHours];
        for(int g = 1; g <= nGenerators; g++){
            for(int h = 1; h <= nHours; h++){
                minUpTimeAtH[g-1][h-1] = Math.min(h+minUpTime[g-1]-1, nHours);
                minDownTimeAtH[g-1][h-1] = Math.min(h+minDownTime[g-1]-1, nHours);
            }
        }
        
        double[] sheddingCosts = new double[nHours];
        double maxProductionCost = 0;
        for(int h = 1; h <= nHours; h++){
             if (productionCost[h-1] > maxProductionCost){
                 maxProductionCost=productionCost[h-1];
             }
        }
        for(int h = 1; h <= nHours; h++){
            sheddingCosts[h-1]=2*maxProductionCost; 
        }
        
        // 3. CREATES AN INSTANCE OF THE UNIT COMMITMENT PROBLEM
        
        UnitCommitmentProblem ucp 
                = new UnitCommitmentProblem(nHours,nGenerators,
                        names,commitmentCosts,productionCost,
                        sheddingCosts, startUpCosts,
                        minUpTime,minDownTime,
                        minUpTimeAtH,minDownTimeAtH,
                        minProduction,maxProduction,
                        rampUp,rampDown,
                        demand
        );
        
        // 4. CREATES THE MATHEMATICAL MODEL AND SOLVES IT (WITHOUT DECOMPOSITION)
        UnitCommitmentModel ucpm = new UnitCommitmentModel(ucp);
        ucpm.solve();
        ucpm.printSolution();
        
        
        // 5. SOLVES THE PROBLEM USING BENDERS DECOMPOSITION
     
        
        // We create the Master Problem
        MasterProblem mp = new MasterProblem(ucp);
        mp.solve();
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        mp.printSolution();
       
        // and check to see if the results are consistent with solving the full problem
        System.out.println("=======================");
        System.out.println("SOLUTION WITHOUT DECOMPOSITION");
        System.out.println("=======================");
        UnitCommitmentModel m = new UnitCommitmentModel(ucp);
        m.solve();
        
        
    
    }
    
    
}

